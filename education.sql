/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : education

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 07/11/2019 19:16:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for edu_college
-- ----------------------------
DROP TABLE IF EXISTS `edu_college`;
CREATE TABLE `edu_college`  (
  `id` int(11) NOT NULL,
  `c_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学院名',
  `c_num` char(0) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学院编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for edu_course
-- ----------------------------
DROP TABLE IF EXISTS `edu_course`;
CREATE TABLE `edu_course`  (
  `id` int(11) NOT NULL,
  `c_num` char(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '课程编号',
  `c_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '课程姓名',
  `c_credit` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学分',
  `c_percourse` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '角色',
  `c_zxs` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '周学时',
  `c_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '选/必修',
  `c_volume` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '课容量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of edu_course
-- ----------------------------
INSERT INTO `edu_course` VALUES (1, 'R1001', '软件工程', '4', NULL, '12', '1', '40');
INSERT INTO `edu_course` VALUES (2, 'R1002', '计算机网络', '4', NULL, '15', '1', '30');
INSERT INTO `edu_course` VALUES (3, 'R1003', '离散数学', '4', NULL, '12', '1', '40');
INSERT INTO `edu_course` VALUES (4, 'R1004', '数据库', '4', NULL, '14', '1', '35');

-- ----------------------------
-- Table structure for edu_major
-- ----------------------------
DROP TABLE IF EXISTS `edu_major`;
CREATE TABLE `edu_major`  (
  `id` int(11) NOT NULL,
  `m_num` char(0) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '专业编号',
  `m_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '专业名',
  `m_college` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学院',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for edu_plan
-- ----------------------------
DROP TABLE IF EXISTS `edu_plan`;
CREATE TABLE `edu_plan`  (
  `id` int(11) NOT NULL,
  `p_planName` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '计划课程名称(example:2019年春学期计划表)',
  `p_num` char(11) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '课程编号',
  `p_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '课程名',
  `p_year` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学期',
  `p_majorId` int(11) NULL DEFAULT NULL COMMENT '专业',
  `workday` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '周几',
  `AM` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL,
  `PM` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL,
  `p_tname` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '选课后老师姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of edu_plan
-- ----------------------------
INSERT INTO `edu_plan` VALUES (1, '软件工程', 'YR1001', '软件工程', '2016', 1, '12', '上午', '无', NULL);

-- ----------------------------
-- Table structure for edu_student
-- ----------------------------
DROP TABLE IF EXISTS `edu_student`;
CREATE TABLE `edu_student`  (
  `id` int(11) NOT NULL,
  `s_num` int(11) NULL DEFAULT NULL COMMENT '学号',
  `s_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '姓名',
  `s_sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '性别',
  `s_date` datetime(0) NULL DEFAULT NULL COMMENT '入学年份',
  `s_college` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学院',
  `s_major` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '专业',
  `s_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '班级',
  `s_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '是否留级',
  `s_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for edu_teacher
-- ----------------------------
DROP TABLE IF EXISTS `edu_teacher`;
CREATE TABLE `edu_teacher`  (
  `id` int(11) NOT NULL,
  `t_num` char(11) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '教师工号',
  `t_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '密码',
  `t_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '名字',
  `t_det` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '角色',
  `t_sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '性别',
  `t_age` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '年龄',
  `t_college` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '学院',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of edu_teacher
-- ----------------------------
INSERT INTO `edu_teacher` VALUES (1, '1001', '1001', '张老师', '1', '男', '20', '软件工程');

-- ----------------------------
-- Table structure for tea_stu
-- ----------------------------
DROP TABLE IF EXISTS `tea_stu`;
CREATE TABLE `tea_stu`  (
  `id` int(11) NOT NULL,
  `tt_num` int(11) NULL DEFAULT NULL COMMENT '教师-教学计划之后-学生选课/编号',
  `s_num` int(11) NULL DEFAULT NULL COMMENT '学生编号',
  `备用` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL,
  `备用1` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL,
  `备用2` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tea_tea
-- ----------------------------
DROP TABLE IF EXISTS `tea_tea`;
CREATE TABLE `tea_tea`  (
  `id` int(11) NOT NULL,
  `t_num` int(11) NULL DEFAULT NULL COMMENT '教师编号',
  `c_num` char(0) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '课程编号(冗余-方便)',
  `s_time` datetime(0) NULL DEFAULT NULL COMMENT '上课时间',
  `p_num` char(0) CHARACTER SET utf8 COLLATE utf8_german2_ci NULL DEFAULT NULL COMMENT '教学计划编号',
  `tt_num` int(11) NULL DEFAULT NULL COMMENT '关联表编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
