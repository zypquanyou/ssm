package com.qcby.service.Impl;

import com.qcby.dao.CourseMapper;
import com.qcby.entity.Course;
import com.qcby.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("courseService")
public class CourseServiceImpl implements CourseService {
 @Autowired
    private CourseMapper courseMapper;

    @Override
    public List<Course> findList(Course course) {

        return courseMapper.findList(course);
    }

}
