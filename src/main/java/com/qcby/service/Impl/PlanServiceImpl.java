package com.qcby.service.Impl;

import com.qcby.dao.PlanMapper;
import com.qcby.entity.Plan;
import com.qcby.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("planService")
public class PlanServiceImpl implements PlanService {
    @Autowired
    private PlanMapper planMapper;
    @Override
    public List<Plan> planList(Plan plan){
        return planMapper.planList(plan);
    }

    @Override
    public int insertPlan(Plan plan) {
        return planMapper.insertPlan(plan);
    }


}
