package com.qcby.service;

import com.qcby.entity.Plan;

import java.util.List;

public interface PlanService {
    List<Plan> planList(Plan plan);
    int insertPlan(Plan plan);
}
