package com.qcby.controller;


import com.qcby.dao.CourseMapper;
import com.qcby.entity.Course;


import com.qcby.entity.Plan;
import com.qcby.service.CourseService;
import com.qcby.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;

@Controller
@RequestMapping("course")
public class CourseController {
    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private CourseService courseService;

    @Autowired
    private PlanService planService;



    /**
     * 课表查询
     * @param model
     * @param course
     * @return
     */
    @RequestMapping("list")
    public String list(Model model, Course course) {
        List<Course> cour = courseService.findList(course);
        model.addAttribute("data", cour);
        return "course";
    }

    /**
     * 计划表查询
     * @param model
     * @param plan
     * @return
     */
    @RequestMapping("listPlan")
    public String listPlan(Model model, Plan plan){
        List<Plan> pl = planService.planList(plan);
        model.addAttribute("data",pl);
        return "plan";
    }

    @RequestMapping("selectPlan")
    public String selectPlan(Model model,Course course){
        Course course1 = courseMapper.selectByPrimaryKey(course.getId());
        model.addAttribute("data",course1);
        return "form";
    }

    /**
     * 老师选课添加内容
     *
     * @par
     * @return
     */
        @RequestMapping("insertPlan")
        public String insertPlan(Model model, Plan plan,String name,String num){
       int byTag = planService.insertPlan(plan);
       return null;
    }
}

