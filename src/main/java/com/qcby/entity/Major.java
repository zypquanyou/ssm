package com.qcby.entity;

public class Major {
    private Integer id;

    private String m_num;

    private String m_name;

    private String m_college;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getM_num() {
        return m_num;
    }

    public void setM_num(String m_num) {
        this.m_num = m_num == null ? null : m_num.trim();
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name == null ? null : m_name.trim();
    }

    public String getM_college() {
        return m_college;
    }

    public void setM_college(String m_college) {
        this.m_college = m_college == null ? null : m_college.trim();
    }
}