package com.qcby.entity;

public class Course {
    private Integer id;

    private String c_num;

    private String c_name;

    private String c_credit;

    private String c_zxs;

    private String c_type;

    private String c_volume;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getC_num() {
        return c_num;
    }

    public void setC_num(String c_num) {
        this.c_num = c_num == null ? null : c_num.trim();
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name == null ? null : c_name.trim();
    }

    public String getC_credit() {
        return c_credit;
    }

    public void setC_credit(String c_credit) {
        this.c_credit = c_credit == null ? null : c_credit.trim();
    }

    public String getC_zxs() {
        return c_zxs;
    }

    public void setC_zxs(String c_zxs) {
        this.c_zxs = c_zxs == null ? null : c_zxs.trim();
    }

    public String getC_type() {
        return c_type;
    }

    public void setC_type(String c_type) {
        this.c_type = c_type == null ? null : c_type.trim();
    }

    public String getC_volume() {
        return c_volume;
    }

    public void setC_volume(String c_volume) {
        this.c_volume = c_volume == null ? null : c_volume.trim();
    }
}