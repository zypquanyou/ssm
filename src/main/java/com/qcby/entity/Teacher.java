package com.qcby.entity;

public class Teacher {
    private Integer id;

    private String t_num;

    private String t_password;

    private String t_name;

    private String t_det;

    private String t_sex;

    private String t_age;

    private String t_college;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getT_num() {
        return t_num;
    }

    public void setT_num(String t_num) {
        this.t_num = t_num == null ? null : t_num.trim();
    }

    public String getT_password() {
        return t_password;
    }

    public void setT_password(String t_password) {
        this.t_password = t_password == null ? null : t_password.trim();
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name == null ? null : t_name.trim();
    }

    public String getT_det() {
        return t_det;
    }

    public void setT_det(String t_det) {
        this.t_det = t_det == null ? null : t_det.trim();
    }

    public String getT_sex() {
        return t_sex;
    }

    public void setT_sex(String t_sex) {
        this.t_sex = t_sex == null ? null : t_sex.trim();
    }

    public String getT_age() {
        return t_age;
    }

    public void setT_age(String t_age) {
        this.t_age = t_age == null ? null : t_age.trim();
    }

    public String getT_college() {
        return t_college;
    }

    public void setT_college(String t_college) {
        this.t_college = t_college == null ? null : t_college.trim();
    }
}