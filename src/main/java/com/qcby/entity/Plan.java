package com.qcby.entity;

public class Plan {
    private Integer id;

    private String p_planName;

    private String p_num;

    private String p_name;

    private String p_year;

    private Integer p_majorId;

    private String workday;

    private String AM;

    private String PM;

    private String t_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getP_planName() {
        return p_planName;
    }

    public void setP_planName(String p_planName) {
        this.p_planName = p_planName == null ? null : p_planName.trim();
    }

    public String getP_num() {
        return p_num;
    }

    public void setP_num(String p_num) {
        this.p_num = p_num == null ? null : p_num.trim();
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name == null ? null : p_name.trim();
    }

    public String getP_year() {
        return p_year;
    }

    public void setP_year(String p_year) {
        this.p_year = p_year == null ? null : p_year.trim();
    }

    public Integer getP_majorId() {
        return p_majorId;
    }

    public void setP_majorId(Integer p_majorId) {
        this.p_majorId = p_majorId;
    }

    public String getWorkday() {
        return workday;
    }

    public void setWorkday(String workday) {
        this.workday = workday == null ? null : workday.trim();
    }

    public String getAM() {
        return AM;
    }

    public void setAM(String AM) {
        this.AM = AM == null ? null : AM.trim();
    }

    public String getPM() {
        return PM;
    }

    public void setPM(String PM) {
        this.PM = PM == null ? null : PM.trim();
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id == null ? null : t_id.trim();
    }

    @Override
    public String toString() {
        return "Plan{" +
                "id=" + id +
                ", p_planName='" + p_planName + '\'' +
                ", p_num='" + p_num + '\'' +
                ", p_name='" + p_name + '\'' +
                ", p_year='" + p_year + '\'' +
                ", p_majorId=" + p_majorId +
                ", workday='" + workday + '\'' +
                ", AM='" + AM + '\'' +
                ", PM='" + PM + '\'' +
                ", t_id='" + t_id + '\'' +
                '}';
    }
}