package com.qcby.entity;

public class Tea_stu {
    private Integer id;

    private Integer c_num;

    private Integer s_num;

    private String 备用;

    private String 备用1;

    private String 备用2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getC_num() {
        return c_num;
    }

    public void setC_num(Integer c_num) {
        this.c_num = c_num;
    }

    public Integer getS_num() {
        return s_num;
    }

    public void setS_num(Integer s_num) {
        this.s_num = s_num;
    }

    public String get备用() {
        return 备用;
    }

    public void set备用(String 备用) {
        this.备用 = 备用 == null ? null : 备用.trim();
    }

    public String get备用1() {
        return 备用1;
    }

    public void set备用1(String 备用1) {
        this.备用1 = 备用1 == null ? null : 备用1.trim();
    }

    public String get备用2() {
        return 备用2;
    }

    public void set备用2(String 备用2) {
        this.备用2 = 备用2 == null ? null : 备用2.trim();
    }
}