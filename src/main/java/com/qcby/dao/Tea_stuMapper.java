package com.qcby.dao;

import com.qcby.entity.Tea_stu;

public interface Tea_stuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Tea_stu record);

    int insertSelective(Tea_stu record);

    Tea_stu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Tea_stu record);

    int updateByPrimaryKey(Tea_stu record);
}