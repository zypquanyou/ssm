<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 11055
  Date: 2019/11/8
  Time: 8:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>课程表</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row">
    <h1 align="center">教师选课表</h1>
    <div class="col-md-8 col-md-offset-2">
        <table class="table">
            <tr>
                <th>课程编号</th>
                <th>课程姓名</th>
                <th>学分</th>
<%--                <th>角色</th>--%>
                <th>周学时</th>
                <th>选/必修</th>
                <th>课容量</th>
            </tr>
            <c:forEach items="${data}" var="c">
                <tr>
                    <td>${c.c_num}</td>
                    <td>${c.c_name}</td>
                    <td>${c.c_credit}</td>
<%--                    <td>${c.c_percourse}</td>--%>
                    <td>${c.c_zxs}</td>
                    <td>${c.c_type}</td>
                    <td>${c.c_volume}</td>
                    <td>
                        <button onclick="updatePlan111(${c.id})" class="btn btn-primary">选课</button>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <div>
            <a href="../course/listPlan"><h4>老师课表</h4></a>
        </div>
    </div>
</div>
</body>
<script>
    function updatePlan111(id) {
        window.location.href="../course/selectPlan?id="+id;
    }
</script>

</html>
