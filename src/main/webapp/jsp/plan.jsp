<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 11055
  Date: 2019/11/8
  Time: 14:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>老师课表</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row">
    <h1 align="center">教学计划表</h1>
    <div class="col-md-8 col-md-offset-2">
        <table class="table">
            <tr>
                <th>课程计划名称</th>
                <th>课程编号</th>
                <th>课程名</th>
                <th>学期</th>
                <th>专业</th>
                <th>星期</th>
                <th>上午</th>
                <th>下午</th>
            </tr>
            <c:forEach items="${data}" var="p">
            <tr>
                <td>${p.p_planName}</td>
                <td>${p.p_num}</td>
                <td>${p.p_name}</td>
                <td>${p.p_year}</td>
                <td>${p.p_majorId}</td>
                <td>${p.workday}</td>
                <td>${p.AM}</td>
                <td>${p.PM}</td>
<%--                <td>--%>
<%--                    <button onclick="updatePlan(${p.id})" class="btn btn-primary">更改</button>--%>
<%--                    <button onclick="deteletPlan(${p.id})" class="btn btn-primary">删除</button>--%>
<%--                </td>--%>
            </tr>
            </c:forEach>
</body>
<%--<script>--%>
<%--    function deteletPlan(id){--%>
<%--        window.location.href="?id"+id;--%>
<%--    }--%>
<%--</script>--%>
</html>
